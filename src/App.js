import React from 'react';

import './App.css';
import DropdownList from './dropdown-list/DropdownList';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      searchValue: '',
      listData: []
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({searchValue: event.target.value});
    fetch('http://api.giphy.com/v1/gifs/search?api_key=yxj6PvsXYAvVAOHM3zfHn7ni2XEbxL27&q=' + event.target.value)
    .then(res => res.json())
    .then(response => {
      this.setState({listData: response.data});
    })
  }

  render() {
    return (
      <div className="App">
        <input placeholder="Search" className="search-input" value={this.state.searchValue} onChange={this.handleChange}/>
        <DropdownList list={this.state.listData}/>
      </div>
    );
  }
}

export default App;

import React from 'react';


class DropdownList extends React.Component {
  constructor() {
    super();
  }

  copyCodeToClipboard = (index) => {
    const el = this['textToCopy'+index];
    el.select();
    document.execCommand("copy");
  }

  render() {
    return (
      <div>
        {
            this.props.list.map((item, index) => {
                return (
                    <div key={'listItem' +index} className="listItem">
                        <div>
                            <img className="image" src={item.images.fixed_height_small.url}></img>
                        </div>
                        <div >
                            <div className="title">{item.title}</div>
                            <div className="buttons">
                                <button onClick={() => this.copyCodeToClipboard(index)}>Copy</button>
                                <button onClick={() => window.location.href = item.images.fixed_height_small.url}>Redirect</button>
                            </div>
                        </div>
                        <textarea ref={(textToCopy) => this['textToCopy'+index] = textToCopy} value={item.images.fixed_height_small.url}></textarea>
                    </div>
                )
            })
        }
      </div>
    );
  }
}

export default DropdownList;
